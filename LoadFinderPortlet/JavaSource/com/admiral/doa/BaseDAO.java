/*
 * Created on Apr 27, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.admiral.doa;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;

/**
 * @author enord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BaseDAO {
	protected String sortByProperty = "changeMe";
	
	protected transient final Comparator propertyAscendingComparator = new Comparator() {
        public int compare(Object object1, Object object2) {
            try {
                String property1 = BeanUtils.getProperty(object1,
                		BaseDAO.this.sortByProperty
                    );
                String property2 = BeanUtils.getProperty(object2,
                		BaseDAO.this.sortByProperty
                    );

                return property1.toLowerCase().compareTo(property2.toLowerCase());
            } catch (Exception e) {
                return 0;
            }
        }
    };

    protected transient final Comparator propertyDescendingComparator = new Comparator() {
        public int compare(Object object1, Object object2) {
            try {
                String property1 = BeanUtils.getProperty(object1,
                		BaseDAO.this.sortByProperty
                    );
                String property2 = BeanUtils.getProperty(object2,
                		BaseDAO.this.sortByProperty
                    );

                return property2.toLowerCase().compareTo(property1.toLowerCase());
            } catch (Exception e) {
                return 0;
            }
        }
    };
    
    public Connection getConnection(String username, String password) {
		try {
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.ibm.Websphere.naming.WsnInitialContextFactory");
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/admiral_connection");
			//DataSource ds = (DataSource) ctx.lookup("jdbc/" + getProperty("jndiname"));
			return ds.getConnection(username, password);
		} catch(NamingException ne) {
			ne.printStackTrace();
			return null;
		} catch(SQLException se) {
			se.printStackTrace();
			return null;
		}
	}
	
	protected Properties getDataProperties() {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream(new File("data.properties")));
			return properties;
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}
	
	protected String getProperty(String myProperty) {
		return getDataProperties().getProperty(myProperty);
	}
	
	protected void sort(Comparator comparator, List data) {
        Collections.sort(data, comparator);
    }

}
