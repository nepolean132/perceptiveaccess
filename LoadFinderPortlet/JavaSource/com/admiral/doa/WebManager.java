/*
 * Created on Apr 27, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.admiral.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.admiral.bean.LoadFinderBean;

/**
 * @author enord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WebManager extends BaseDAO {
	public int getNumberOfTotalLoads() {
		String sql = "select count(*) from webdata.pordrweb01";
		
		Connection conn = getConnection("temp1", "temp1");
		Statement statement = null;
		ResultSet rs = null;
		
		try {
			statement = conn.createStatement();
			
			rs = statement.executeQuery(sql);
			if(rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch(SQLException se) {
			se.printStackTrace();
			return 0;
		} finally {
			try {
				rs.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				statement.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				conn.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
		}
	}
	
	public int getNumberOfTodaysLoads() {
		java.util.Calendar today = java.util.Calendar.getInstance();
		int todaysYear = Integer.parseInt(String.valueOf(today.get(java.util.Calendar.YEAR)).substring(2));
		int todaysMonth = today.get(java.util.Calendar.MONTH) + 1;
		int todaysDate = today.get(java.util.Calendar.DAY_OF_MONTH);
		String sqlDate = "1";
		if(todaysYear < 10) {
			sqlDate = sqlDate + "0" + todaysYear;
		} else {
			sqlDate += todaysYear;
		}
		if(todaysMonth < 10) {
			sqlDate = sqlDate + "0" + todaysMonth;
		} else {
			sqlDate += todaysMonth;
		}
		if(todaysDate < 10) {
			sqlDate = sqlDate + "0" + todaysDate;
		} else {
			sqlDate += todaysDate;
		}
		
		String sql = "select count(*) from webdata.pordrweb01 where POAEDT = ?";
		
		Connection conn = getConnection("temp1", "temp1");
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		try {
			statement = conn.prepareStatement(sql);
			statement.setInt(1, Integer.parseInt(sqlDate));
			rs = statement.executeQuery();
			if(rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch(SQLException se) {
			se.printStackTrace();
			return 0;
		} finally {
			try {
				rs.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				statement.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				conn.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
		}
	}
	
	public ArrayList getReportData() {		
		String sql = "select * from webdata.pordrweb01 order by POAOS";
		
		return runIt(sql);		
	}
	
	protected ArrayList runIt(String sqlStatement) {
		Connection conn = getConnection("temp1", "temp1");
		Statement statement = null;
		ResultSet rs = null;
		
		try {
			statement = conn.createStatement();
			
			rs = statement.executeQuery(sqlStatement);
			return buildArrayList(rs);
		} catch(SQLException se) {
			se.printStackTrace();
			return null;
		} finally {
			try {
				rs.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				statement.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
			try {
				conn.close();
			} catch(SQLException se) {
				se.printStackTrace();
			}
		}
	}
	
	protected ArrayList buildArrayList(ResultSet rs) throws SQLException {
		
		
        
		ArrayList data = new ArrayList();
		LoadFinderBean bean = null;
		StringBuffer tempDate = null;
		while(rs.next()) {
			try {
				bean = new LoadFinderBean();
				tempDate = new StringBuffer(10);
				tempDate.append(rs.getString("DATMDY"));
				if(tempDate.length() == 5) {
					tempDate.insert(0, '0');
				}
				tempDate.insert(2, '/');
				tempDate.insert(5, '/');
				bean.setDate(tempDate.toString());
				bean.setOriginCity(rs.getString("POAOC"));
				bean.setOriginState(rs.getString("POAOS"));
				bean.setDestinationCity(rs.getString("POADC"));
				bean.setDestinationState(rs.getString("POADS"));
				bean.setCommodityCode(rs.getString("POACOD"));
				//String agentEquipId = rs.getString("POACMS");
				//bean.setAgent(agentEquipId.substring(0, 5));
				//bean.setEquipmentId(agentEquipId.substring(6));
				bean.setAgent(rs.getString("AGENT"));
				bean.setEquipmentId(rs.getString("EQUIPID"));
				// System.out.println("Rate Value is *******"+rs.getString("POART"));
				// System.out.println("Rate Value substring is *******"+rs.getString("POART").substring(0, rs.getString("POART").indexOf(".") + 3).replaceAll("(\\.00$)", ""));
				bean.setRate(rs.getString("POART").substring(0, rs.getString("POART").indexOf(".") + 3).replaceAll("(\\.00$)", ""));
				bean.setRateBasisCode(rs.getString("POARU"));
				bean.setDetail1(rs.getString("POACM1"));
				bean.setDetail2(rs.getString("POACM2"));
				String agentPhone = rs.getString("CTAPPN");
				if(agentPhone.length() >= 10) {
					StringBuffer buildPhoneNumber = new StringBuffer(20);
					buildPhoneNumber.append(agentPhone.substring(0, 3))
						.append("-").append(agentPhone.substring(3, 6))
						.append("-").append(agentPhone.substring(6));
					bean.setAgentPhone(buildPhoneNumber.toString());
				} else {
					bean.setAgentPhone("");
				}
				bean.setEnteredDate(rs.getInt("POAEDT"));
				data.add(bean);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}
	
	public ArrayList getReportDataByDate(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by DATMDY " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByOriginCity(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POAOC " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByOriginState(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POAOS " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByDestinationCity(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POADC " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByDestinationState(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POADS " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByCommodityCode(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POACOD " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByAgent(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by AGENT " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByEquipmentId(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by EQUIPID " + mode;
		
		//ArrayList thisArray = runIt(sql);
		
		//sortByProperty = "equipmentId";
        //sort(propertyAscendingComparator, thisArray);
		
		//return thisArray;
		return runIt(sql);
	}
	
	public ArrayList getReportDataByRate(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POART " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByRateBasisCode(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POARU " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByDetail1(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POACM1 " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByDetail2(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by POACM2 " + mode;
		
		return runIt(sql);		
	}
	
	public ArrayList getReportDataByAgentPhone(String mode) {		
		String sql = "select * from webdata.pordrweb01 order by CTAPPN " + mode;
		
		return runIt(sql);		
	}
}
