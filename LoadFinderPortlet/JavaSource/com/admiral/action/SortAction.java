/*
 * Created on Sep 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.admiral.action;

import java.util.ArrayList;

import com.admiral.bean.SortBean;
import com.admiral.doa.WebManager;

/**
 * @author enord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SortAction {
	private WebManager manager = new WebManager();

	public ArrayList sortByDate(SortBean bean) {
		if((!bean.isDateAsc() && !bean.isDateDec()) || (bean.isDateDec())) {
			// do Ascending		
			bean.reset();
			bean.setDateAsc(true);
			return manager.getReportDataByDate("ASC");
		} else if(bean.isDateAsc()) {
			// do Descending
			bean.reset();
			bean.setDateDec(true);
			return manager.getReportDataByDate("DESC");
		}
		return null;
	}
	
	public ArrayList sortByOriginCity(SortBean bean) {
		if((!bean.isOriginCityAsc() && !bean.isOriginCityDec()) || (bean.isOriginCityDec())) {
			// do Ascending		
			bean.reset();
			bean.setOriginCityAsc(true);
			return manager.getReportDataByOriginCity("ASC");
		} else if(bean.isOriginCityAsc()) {
			// do Descending
			bean.reset();
			bean.setOriginCityDec(true);
			return manager.getReportDataByOriginCity("DESC");
		}
		return null;
	}
	
	public ArrayList sortByOriginState(SortBean bean) {
		if((!bean.isOriginStateAsc() && !bean.isOriginStateDec()) || (bean.isOriginStateDec())) {
			// do Ascending		
			bean.reset();
			bean.setOriginStateAsc(true);
			return manager.getReportDataByOriginState("ASC");
		} else if(bean.isOriginStateAsc()) {
			// do Descending
			bean.reset();
			bean.setOriginStateDec(true);
			return manager.getReportDataByOriginState("DESC");
		}
		return null;
	}
	
	public ArrayList sortByDestinationCity(SortBean bean) {
		if((!bean.isDestinationCityAsc() && !bean.isDestinationCityDec()) || (bean.isDestinationCityDec())) {
			// do Ascending		
			bean.reset();
			bean.setDestinationCityAsc(true);
			return manager.getReportDataByDestinationCity("ASC");
		} else if(bean.isDestinationCityAsc()) {
			// do Descending
			bean.reset();
			bean.setDestinationCityDec(true);
			return manager.getReportDataByDestinationCity("DESC");
		}
		return null;
	}
	
	public ArrayList sortByDestinationState(SortBean bean) {
		if((!bean.isDestinationStateAsc() && !bean.isDestinationStateDec()) || (bean.isDestinationStateDec())) {
			// do Ascending		
			bean.reset();
			bean.setDestinationStateAsc(true);
			return manager.getReportDataByDestinationState("ASC");
		} else if(bean.isDestinationStateAsc()) {
			// do Descending
			bean.reset();
			bean.setDestinationStateDec(true);
			return manager.getReportDataByDestinationState("DESC");
		}
		return null;
	}
	
	public ArrayList sortByCommodityCode(SortBean bean) {
		if((!bean.isCommodityCodeAsc() && !bean.isCommodityCodeDec()) || (bean.isCommodityCodeDec())) {
			// do Ascending		
			bean.reset();
			bean.setCommodityCodeAsc(true);
			return manager.getReportDataByCommodityCode("ASC");
		} else if(bean.isCommodityCodeAsc()) {
			// do Descending
			bean.reset();
			bean.setCommodityCodeDec(true);
			return manager.getReportDataByCommodityCode("DESC");
		}
		return null;
	}
	
	public ArrayList sortByAgent(SortBean bean) {
		if((!bean.isAgentAsc() && !bean.isAgentDec()) || (bean.isAgentDec())) {
			// do Ascending		
			bean.reset();
			bean.setAgentAsc(true);
			return manager.getReportDataByAgent("ASC");
		} else if(bean.isAgentAsc()) {
			// do Descending
			bean.reset();
			bean.setAgentDec(true);
			return manager.getReportDataByAgent("DESC");
		}
		return null;
	}
	
	public ArrayList sortByEquipmentId(SortBean bean) {
		if((!bean.isEquipmentIdAsc() && !bean.isEquipmentIdDec()) || (bean.isEquipmentIdDec())) {
			// do Ascending		
			bean.reset();
			bean.setEquipmentIdAsc(true);
			return manager.getReportDataByEquipmentId("ASC");
		} else if(bean.isEquipmentIdAsc()) {
			// do Descending
			bean.reset();
			bean.setEquipmentIdDec(true);
			return manager.getReportDataByEquipmentId("DESC");
		}
		return null;
	}
	
	public ArrayList sortByRate(SortBean bean) {
		if((!bean.isRateAsc() && !bean.isRateDec()) || (bean.isRateDec())) {
			// do Ascending		
			bean.reset();
			bean.setRateAsc(true);
			return manager.getReportDataByRate("ASC");
		} else if(bean.isRateAsc()) {
			// do Descending
			bean.reset();
			bean.setRateDec(true);
			return manager.getReportDataByRate("DESC");
		}
		return null;
	}
	
	public ArrayList sortByRateBasisCode(SortBean bean) {
		if((!bean.isRateBasisCodeAsc() && !bean.isRateBasisCodeDec()) || (bean.isRateBasisCodeDec())) {
			// do Ascending		
			bean.reset();
			bean.setRateBasisCodeAsc(true);
			return manager.getReportDataByRateBasisCode("ASC");
		} else if(bean.isRateBasisCodeAsc()) {
			// do Descending
			bean.reset();
			bean.setRateBasisCodeDec(true);
			return manager.getReportDataByRateBasisCode("DESC");
		}
		return null;
	}
	
	public ArrayList sortByDetail1(SortBean bean) {
		if((!bean.isDetail1Asc() && !bean.isDetail1Dec()) || (bean.isDetail1Dec())) {
			// do Ascending		
			bean.reset();
			bean.setDetail1Asc(true);
			return manager.getReportDataByDetail1("ASC");
		} else if(bean.isDetail1Asc()) {
			// do Descending
			bean.reset();
			bean.setDetail1Dec(true);
			return manager.getReportDataByDetail1("DESC");
		}
		return null;
	}
	
	public ArrayList sortByDetail2(SortBean bean) {
		if((!bean.isDetail2Asc() && !bean.isDetail2Dec()) || (bean.isDetail2Dec())) {
			// do Ascending		
			bean.reset();
			bean.setDetail2Asc(true);
			return manager.getReportDataByDetail2("ASC");
		} else if(bean.isDetail2Asc()) {
			// do Descending
			bean.reset();
			bean.setDetail2Dec(true);
			return manager.getReportDataByDetail2("DESC");
		}
		return null;
	}
	
	public ArrayList sortByAgentPhone(SortBean bean) {
		if((!bean.isAgentPhoneAsc() && !bean.isAgentPhoneDec()) || (bean.isAgentPhoneDec())) {
			// do Ascending		
			bean.reset();
			bean.setAgentPhoneAsc(true);
			return manager.getReportDataByAgentPhone("ASC");
		} else if(bean.isAgentPhoneAsc()) {
			// do Descending
			bean.reset();
			bean.setAgentPhoneDec(true);
			return manager.getReportDataByAgentPhone("DESC");
		}
		return null;
	}
}
