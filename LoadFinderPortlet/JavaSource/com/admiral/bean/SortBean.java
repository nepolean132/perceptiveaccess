/*
 * Created on Sep 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.admiral.bean;

/**
 * @author enord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SortBean {

	private boolean dateAsc = false;
	private boolean dateDec = false;
	private boolean originCityAsc = false;
	private boolean originCityDec = false;
	private boolean originStateAsc = false;
	private boolean originStateDec = false;
	private boolean destinationCityAsc = false;
	private boolean destinationCityDec = false;
	private boolean destinationStateAsc = false;
	private boolean destinationStateDec = false;
	private boolean commodityCodeAsc = false;
	private boolean commodityCodeDec = false;
	private boolean agentAsc = false;
	private boolean agentDec = false;
	private boolean equipmentIdAsc = false;
	private boolean equipmentIdDec = false;
	private boolean rateAsc = false;
	private boolean rateDec = false;
	private boolean rateBasisCodeAsc = false;
	private boolean rateBasisCodeDec = false;
	private boolean detail1Asc = false;
	private boolean detail1Dec = false;
	private boolean detail2Asc = false;
	private boolean detail2Dec = false;
	private boolean agentPhoneAsc = false;
	private boolean agentPhoneDec = false;
	
	public void reset() {
		dateAsc = false;
		dateDec = false;
		originCityAsc = false;
		originCityDec = false;
		originStateAsc = false;
		originStateDec = false;
		destinationCityAsc = false;
		destinationCityDec = false;
		destinationStateAsc = false;
		destinationStateDec = false;
		commodityCodeAsc = false;
		commodityCodeDec = false;
		agentAsc = false;
		agentDec = false;
		equipmentIdAsc = false;
		equipmentIdDec = false;
		rateAsc = false;
		rateDec = false;
		rateBasisCodeAsc = false;
		rateBasisCodeDec = false;
		detail1Asc = false;
		detail1Dec = false;
		detail2Asc = false;
		detail2Dec = false;
	}
	
	public static void main(String[] args) {
	}
	
	/**
	 * @return Returns the agentAsc.
	 */
	public boolean isAgentAsc() {
		return agentAsc;
	}
	/**
	 * @param agentAsc The agentAsc to set.
	 */
	public void setAgentAsc(boolean agentAsc) {
		this.agentAsc = agentAsc;
	}
	/**
	 * @return Returns the agentDec.
	 */
	public boolean isAgentDec() {
		return agentDec;
	}
	/**
	 * @param agentDec The agentDec to set.
	 */
	public void setAgentDec(boolean agentDec) {
		this.agentDec = agentDec;
	}
	/**
	 * @return Returns the commodityCodeAsc.
	 */
	public boolean isCommodityCodeAsc() {
		return commodityCodeAsc;
	}
	/**
	 * @param commodityCodeAsc The commodityCodeAsc to set.
	 */
	public void setCommodityCodeAsc(boolean commodityCodeAsc) {
		this.commodityCodeAsc = commodityCodeAsc;
	}
	/**
	 * @return Returns the commodityCodeDec.
	 */
	public boolean isCommodityCodeDec() {
		return commodityCodeDec;
	}
	/**
	 * @param commodityCodeDec The commodityCodeDec to set.
	 */
	public void setCommodityCodeDec(boolean commodityCodeDec) {
		this.commodityCodeDec = commodityCodeDec;
	}
	/**
	 * @return Returns the destinationCityAsc.
	 */
	public boolean isDestinationCityAsc() {
		return destinationCityAsc;
	}
	/**
	 * @param destinationCityAsc The destinationCityAsc to set.
	 */
	public void setDestinationCityAsc(boolean destinationCityAsc) {
		this.destinationCityAsc = destinationCityAsc;
	}
	/**
	 * @return Returns the destinationCityDec.
	 */
	public boolean isDestinationCityDec() {
		return destinationCityDec;
	}
	/**
	 * @param destinationCityDec The destinationCityDec to set.
	 */
	public void setDestinationCityDec(boolean destinationCityDec) {
		this.destinationCityDec = destinationCityDec;
	}
	/**
	 * @return Returns the destinationStateAsc.
	 */
	public boolean isDestinationStateAsc() {
		return destinationStateAsc;
	}
	/**
	 * @param destinationStateAsc The destinationStateAsc to set.
	 */
	public void setDestinationStateAsc(boolean destinationStateAsc) {
		this.destinationStateAsc = destinationStateAsc;
	}
	/**
	 * @return Returns the destinationStateDec.
	 */
	public boolean isDestinationStateDec() {
		return destinationStateDec;
	}
	/**
	 * @param destinationStateDec The destinationStateDec to set.
	 */
	public void setDestinationStateDec(boolean destinationStateDec) {
		this.destinationStateDec = destinationStateDec;
	}
	/**
	 * @return Returns the detail1Asc.
	 */
	public boolean isDetail1Asc() {
		return detail1Asc;
	}
	/**
	 * @param detail1Asc The detail1Asc to set.
	 */
	public void setDetail1Asc(boolean detail1Asc) {
		this.detail1Asc = detail1Asc;
	}
	/**
	 * @return Returns the detail1Dec.
	 */
	public boolean isDetail1Dec() {
		return detail1Dec;
	}
	/**
	 * @param detail1Dec The detail1Dec to set.
	 */
	public void setDetail1Dec(boolean detail1Dec) {
		this.detail1Dec = detail1Dec;
	}
	/**
	 * @return Returns the detail2Asc.
	 */
	public boolean isDetail2Asc() {
		return detail2Asc;
	}
	/**
	 * @param detail2Asc The detail2Asc to set.
	 */
	public void setDetail2Asc(boolean detail2Asc) {
		this.detail2Asc = detail2Asc;
	}
	/**
	 * @return Returns the detail2Dec.
	 */
	public boolean isDetail2Dec() {
		return detail2Dec;
	}
	/**
	 * @param detail2Dec The detail2Dec to set.
	 */
	public void setDetail2Dec(boolean detail2Dec) {
		this.detail2Dec = detail2Dec;
	}
	/**
	 * @return Returns the equipmentIdAsc.
	 */
	public boolean isEquipmentIdAsc() {
		return equipmentIdAsc;
	}
	/**
	 * @param equipmentIdAsc The equipmentIdAsc to set.
	 */
	public void setEquipmentIdAsc(boolean equipmentIdAsc) {
		this.equipmentIdAsc = equipmentIdAsc;
	}
	/**
	 * @return Returns the equipmentIdDec.
	 */
	public boolean isEquipmentIdDec() {
		return equipmentIdDec;
	}
	/**
	 * @param equipmentIdDec The equipmentIdDec to set.
	 */
	public void setEquipmentIdDec(boolean equipmentIdDec) {
		this.equipmentIdDec = equipmentIdDec;
	}
	/**
	 * @return Returns the originCityAsc.
	 */
	public boolean isOriginCityAsc() {
		return originCityAsc;
	}
	/**
	 * @param originCityAsc The originCityAsc to set.
	 */
	public void setOriginCityAsc(boolean originCityAsc) {
		this.originCityAsc = originCityAsc;
	}
	/**
	 * @return Returns the originCityDec.
	 */
	public boolean isOriginCityDec() {
		return originCityDec;
	}
	/**
	 * @param originCityDec The originCityDec to set.
	 */
	public void setOriginCityDec(boolean originCityDec) {
		this.originCityDec = originCityDec;
	}
	/**
	 * @return Returns the originStateAsc.
	 */
	public boolean isOriginStateAsc() {
		return originStateAsc;
	}
	/**
	 * @param originStateAsc The originStateAsc to set.
	 */
	public void setOriginStateAsc(boolean originStateAsc) {
		this.originStateAsc = originStateAsc;
	}
	/**
	 * @return Returns the originStateDec.
	 */
	public boolean isOriginStateDec() {
		return originStateDec;
	}
	/**
	 * @param originStateDec The originStateDec to set.
	 */
	public void setOriginStateDec(boolean originStateDec) {
		this.originStateDec = originStateDec;
	}
	/**
	 * @return Returns the rateAsc.
	 */
	public boolean isRateAsc() {
		return rateAsc;
	}
	/**
	 * @param rateAsc The rateAsc to set.
	 */
	public void setRateAsc(boolean rateAsc) {
		this.rateAsc = rateAsc;
	}
	/**
	 * @return Returns the rateBasisCodeAsc.
	 */
	public boolean isRateBasisCodeAsc() {
		return rateBasisCodeAsc;
	}
	/**
	 * @param rateBasisCodeAsc The rateBasisCodeAsc to set.
	 */
	public void setRateBasisCodeAsc(boolean rateBasisCodeAsc) {
		this.rateBasisCodeAsc = rateBasisCodeAsc;
	}
	/**
	 * @return Returns the rateBasisCodeDec.
	 */
	public boolean isRateBasisCodeDec() {
		return rateBasisCodeDec;
	}
	/**
	 * @param rateBasisCodeDec The rateBasisCodeDec to set.
	 */
	public void setRateBasisCodeDec(boolean rateBasisCodeDec) {
		this.rateBasisCodeDec = rateBasisCodeDec;
	}
	/**
	 * @return Returns the rateDec.
	 */
	public boolean isRateDec() {
		return rateDec;
	}
	/**
	 * @param rateDec The rateDec to set.
	 */
	public void setRateDec(boolean rateDec) {
		this.rateDec = rateDec;
	}
	/**
	 * @return Returns the dateAsc.
	 */
	public boolean isDateAsc() {
		return dateAsc;
	}
	/**
	 * @param dateAsc The dateAsc to set.
	 */
	public void setDateAsc(boolean dateAsc) {
		this.dateAsc = dateAsc;
	}
	/**
	 * @return Returns the dateDec.
	 */
	public boolean isDateDec() {
		return dateDec;
	}
	/**
	 * @param dateDec The dateDec to set.
	 */
	public void setDateDec(boolean dateDec) {
		this.dateDec = dateDec;
	}
	/**
	 * @return Returns the agentPhoneAsc.
	 */
	public boolean isAgentPhoneAsc() {
		return agentPhoneAsc;
	}
	/**
	 * @param agentPhoneAsc The agentPhoneAsc to set.
	 */
	public void setAgentPhoneAsc(boolean agentPhoneAsc) {
		this.agentPhoneAsc = agentPhoneAsc;
	}
	/**
	 * @return Returns the agentPhoneDec.
	 */
	public boolean isAgentPhoneDec() {
		return agentPhoneDec;
	}
	/**
	 * @param agentPhoneDec The agentPhoneDec to set.
	 */
	public void setAgentPhoneDec(boolean agentPhoneDec) {
		this.agentPhoneDec = agentPhoneDec;
	}
}
