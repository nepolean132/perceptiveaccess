/*
 * Created on Sep 11, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.admiral.bean;

/**
 * @author enord
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LoadFinderBean {
	private String date;
	private String originCity;
	private String originState;
	private String destinationCity;
	private String destinationState;
	private String commodityCode;
	private String agent;
	private String equipmentId;
	private String rate;
	private String rateBasisCode;
	private String detail1;
	private String detail2;
	private String agentPhone;
	private int enteredDate;
	
	/**
	 * @return Returns the date.
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date The date to set.
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * @return Returns the agent.
	 */
	public String getAgent() {
		return agent;
	}
	/**
	 * @param agent The agent to set.
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}
	/**
	 * @return Returns the commodityCode.
	 */
	public String getCommodityCode() {
		return commodityCode;
	}
	/**
	 * @param commodityCode The commodityCode to set.
	 */
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	/**
	 * @return Returns the destinationCity.
	 */
	public String getDestinationCity() {
		return destinationCity;
	}
	/**
	 * @param destinationCity The destinationCity to set.
	 */
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	/**
	 * @return Returns the destinationState.
	 */
	public String getDestinationState() {
		return destinationState;
	}
	/**
	 * @param destinationState The destinationState to set.
	 */
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}
	/**
	 * @return Returns the detail1.
	 */
	public String getDetail1() {
		return detail1;
	}
	/**
	 * @param detail1 The detail1 to set.
	 */
	public void setDetail1(String detail1) {
		this.detail1 = detail1;
	}
	/**
	 * @return Returns the detail2.
	 */
	public String getDetail2() {
		return detail2;
	}
	/**
	 * @param detail2 The detail2 to set.
	 */
	public void setDetail2(String detail2) {
		this.detail2 = detail2;
	}
	/**
	 * @return Returns the equipmentId.
	 */
	public String getEquipmentId() {
		return equipmentId;
	}
	/**
	 * @param equipmentId The equipmentId to set.
	 */
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	/**
	 * @return Returns the originCity.
	 */
	public String getOriginCity() {
		return originCity;
	}
	/**
	 * @param originCity The originCity to set.
	 */
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	/**
	 * @return Returns the originState.
	 */
	public String getOriginState() {
		return originState;
	}
	/**
	 * @param originState The originState to set.
	 */
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	/**
	 * @return Returns the rate.
	 */
	public String getRate() {
		return rate;
	}
	/**
	 * @param rate The rate to set.
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}
	/**
	 * @return Returns the rateBasisCode.
	 */
	public String getRateBasisCode() {
		return rateBasisCode;
	}
	/**
	 * @param rateBasisCode The rateBasisCode to set.
	 */
	public void setRateBasisCode(String rateBasisCode) {
		this.rateBasisCode = rateBasisCode;
	}
	/**
	 * @return Returns the agentPhone.
	 */
	public String getAgentPhone() {
		return agentPhone;
	}
	/**
	 * @param agentPhone The agentPhone to set.
	 */
	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}
	/**
	 * @return Returns the enteredDate.
	 */
	public int getEnteredDate() {
		return enteredDate;
	}
	/**
	 * @param enteredDate The enteredDate to set.
	 */
	public void setEnteredDate(int enteredDate) {
		this.enteredDate = enteredDate;
	}
}
