package loadfinderportlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.apache.jetspeed.portlet.*;
import org.apache.jetspeed.portlet.event.*;

import com.admiral.action.SortAction;
import com.admiral.bean.SortBean;
import com.admiral.doa.WebManager;

/**
 *
 * A sample portlet based on PortletAdapter
 * 
 */
public class LoadFinderPortletPortlet extends PortletAdapter implements ActionListener {

    public static final String VIEW_JSP         = "/loadfinderportlet/jsp/LoadFinderPortletPortletView.";     // JSP file name to be rendered on the view mode
    public static final String SESSION_BEAN     = "loadfinderportlet.LoadFinderPortletPortletSessionBean";    // Bean name for the portlet session
    public static final String FORM_ACTION      = "loadfinderportlet.LoadFinderPortletPortletFormAction";     // Action name for the orderId entry form
    public static final String TEXT             = "loadfinderportlet.LoadFinderPortletPortletText";           // Parameter name for general text input
    public static final String SUBMIT           = "loadfinderportlet.LoadFinderPortletPortletSubmit";         // Parameter name for general submit button
    public static final String CANCEL           = "loadfinderportlet.LoadFinderPortletPortletCancel";         // Parameter name for general cancel button
    
    public static final String HP_PAGE_TARGET	= "/pages/done.jsp";
    public static final String DEFAULT_VIEW_JSP	= "/pages/done.jsp";
    public static final String SHOW_DATA 		= "/pages/done.jsp";
    
    public static HashMap actionMap = new HashMap();
    static {
    	 actionMap.put("done", "/pages/done.jsp");
 
    }

    /**
	 * @see org.apache.jetspeed.portlet.Portlet#init(PortletConfig)
	 */
	public void init(PortletConfig portletConfig) throws UnavailableException {
		super.init(portletConfig);
	}

	/**
	 * @see org.apache.jetspeed.portlet.PortletAdapter#doView(PortletRequest, PortletResponse)
	 */
	public void doView(PortletRequest request, PortletResponse response) throws PortletException, IOException {
		if(request.getSession().getAttribute("sortBean") == null) {
			SortBean sortBean = new SortBean();
			request.getSession().setAttribute("sortBean", sortBean);
		}
		
		HttpSession session = request.getSession(true);
		if(session.getAttribute("data") == null) {
			SortAction sort = new SortAction();
			SortBean sortBean = (SortBean)request.getSession().getAttribute("sortBean");
			ArrayList data = null;
			WebManager web = new WebManager();
			data = web.getReportData();
			session.setAttribute("data", data);
			String jspTarget = (String)request.getAttribute(HP_PAGE_TARGET);
			jspTarget = SHOW_DATA;
			request.getPortletSession().setAttribute(HP_PAGE_TARGET,jspTarget);
			getPortletConfig().getContext().include(jspTarget, request, response);
		} else {
			// Check if portlet session exists
			String jspTarget = (String) request.getAttribute(HP_PAGE_TARGET);
			if(jspTarget==null) { //for a simple refresh
				jspTarget = (String)request.getPortletSession().getAttribute(HP_PAGE_TARGET);
			}
			if(jspTarget==null) { //for the initial portlet view
				jspTarget = DEFAULT_VIEW_JSP;
			}
			request.getPortletSession().setAttribute(HP_PAGE_TARGET,jspTarget);
			getPortletConfig().getContext().include(jspTarget, request, response);
		}
	}

	/**
	 * @see org.apache.jetspeed.portlet.event.ActionListener#actionPerformed(ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) throws PortletException {
		String actionString = dispatchEvent(event);		
        PortletRequest request = event.getRequest();
		if(actionMap.containsKey(actionString)) {		
			request.setAttribute(HP_PAGE_TARGET,actionMap.get(actionString));
		}
	}
	
	public String dispatchEvent(ActionEvent event) {
		//System.out.println("We are in the dispatchEvent method.");
		HttpSession session = event.getRequest().getSession(true);
		WebManager web = new WebManager();
		event.getRequest().getSession().setAttribute("totalLoads", new Integer(web.getNumberOfTotalLoads()));
		event.getRequest().getSession().setAttribute("todaysLoads", new Integer(web.getNumberOfTodaysLoads()));
		
		SortAction sort = new SortAction();
		SortBean sortBean = (SortBean)event.getRequest().getSession().getAttribute("sortBean");
		ArrayList data = null;
		if(event.getActionString().equalsIgnoreCase("date")) {
			data = sort.sortByDate(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("originCity")) {
			data = sort.sortByOriginCity(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("originState")) {
			data = sort.sortByOriginState(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("destinationCity")) {
			data = sort.sortByDestinationCity(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("destinationState")) {
			data = sort.sortByDestinationState(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("commodityCode")) {
			data = sort.sortByCommodityCode(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("agent")) {
			data = sort.sortByAgent(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("equipmentId")) {
			data = sort.sortByEquipmentId(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("rate")) {
			data = sort.sortByRate(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("rateBasisCode")) {
			data = sort.sortByRateBasisCode(sortBean);
		} else if(event.getActionString().equalsIgnoreCase("detail1")) {
			data = sort.sortByDetail1(sortBean);
		} else if(event.getActionString().equals("detail2")){
			data = sort.sortByDetail2(sortBean);
		} else if(event.getActionString().equals("agentPhone")){
			data = sort.sortByAgentPhone(sortBean);
		} else {
			data = web.getReportData();
		}
		
		session.setAttribute("data", data);
		
		return "done";
	}

	/**
	 * Get SessionBean.
	 * 
	 * @param request PortletRequest
	 * @return LoadFinderPortletPortletSessionBean
	 */
	private LoadFinderPortletPortletSessionBean getSessionBean(PortletRequest request) {
        PortletSession session = request.getPortletSession(false);
        if( session == null )
        	return null;
        LoadFinderPortletPortletSessionBean sessionBean = (LoadFinderPortletPortletSessionBean)session.getAttribute(SESSION_BEAN);
        if( sessionBean == null ) {
        	sessionBean = new LoadFinderPortletPortletSessionBean();
        	session.setAttribute(SESSION_BEAN,sessionBean);
        }
        return sessionBean;
	}

	/**
	 * Returns the file extension for the JSP file
	 * 
	 * @param request PortletRequest
	 * @return JSP extension 
	 */
	private static String getJspExtension(PortletRequest request) {
		String markupName = request.getClient().getMarkupName();
		return "jsp";
	}
	
}
