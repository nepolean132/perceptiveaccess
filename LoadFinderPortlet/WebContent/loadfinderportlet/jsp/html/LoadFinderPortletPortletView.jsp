<%@ page session="false" contentType="text/html" import="java.util.*, loadfinderportlet.*"%>
<%@ taglib uri="/WEB-INF/tld/portlet.tld" prefix="portletAPI" %>
<portletAPI:init/>
  
<%
	LoadFinderPortletPortletSessionBean sessionBean = (LoadFinderPortletPortletSessionBean)portletRequest.getPortletSession().getAttribute(LoadFinderPortletPortlet.SESSION_BEAN);
%>

<DIV style="margin: 6px">

<H3 style="margin-bottom: 3px">Welcome!</H3>
This is a sample <B>view mode</B> page. You have to edit this page to customize it for your own use.<BR>
The source file for this page is "/Web Content/loadfinderportlet/jsp/html/LoadFinderPortletPortletView.jsp".

<H3 style="margin-bottom: 3px">Form sample</H3>
This is a sample form to test action event handling.
<DIV style="margin: 12px; margin-bottom: 36px">
<% /******** Start of sample code ********/ %>

  <%
  String formText = sessionBean.getFormText();
  if( formText.length()>0 ) {
    %>Order details for order id '<%=formText%>' should be displayed here.<%
  }
  %>
  <FORM method="POST" action="<portletAPI:createURI><portletAPI:URIAction name='<%=LoadFinderPortletPortlet.FORM_ACTION%>'/></portletAPI:createURI>">
    <LABEL class="wpsLabelText"   for="<portletAPI:encodeNamespace value='<%=LoadFinderPortletPortlet.TEXT%>'/>">Enter order id:</LABEL><BR>
    <INPUT class="wpsEditField"  name="<portletAPI:encodeNamespace value='<%=LoadFinderPortletPortlet.TEXT%>'/>" type="text"/>
    <INPUT class="wpsButtonText" name="<portletAPI:encodeNamespace value='<%=LoadFinderPortletPortlet.SUBMIT%>'/>" value="Submit" type="submit"/>
  </FORM>

<% /******** End of sample code *********/ %>
</DIV>

</DIV>
