<%@page import="javax.swing.text.html.HTML"%>
<%@ taglib uri="/WEB-INF/tld/portlet.tld" prefix="portletAPI"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<HTML>
<HEAD>
<style>
    
    table {
    table-layout:fixed;
    overflow:hidden;
    word-wrap:break-word;
    }
	.st {
    margin-left: auto;
    margin-right: auto;
    width: 4%;
	
}


</style>
<LINK rel="stylesheet"
	href='<%= response.encodeURL("/themes/master.css") %>' type="text/css">  
</HEAD>
<BODY>
<portletAPI:init />
<jsp:useBean id="data" class="java.util.ArrayList"
	type="java.util.ArrayList" scope="session"></jsp:useBean>
<jsp:useBean id="sortBean" class="com.admiral.bean.SortBean"
	type="com.admiral.bean.SortBean" scope="session"></jsp:useBean>
<jsp:useBean id="webData" scope="page" class="com.admiral.doa.WebManager" type="com.admiral.doa.WebManager"></jsp:useBean>
<TABLE border="0"  cellpadding="0" cellspacing="7"  overflow="hidden">
	<TBODY>
		<TR>
			<TH>Total Number of Loads:</TH>
			<TD><%=webData.getNumberOfTotalLoads() %></TD>
		</TR>
		<TR>
			<TH>Total Number of New Loads:</TH>
			<TD><%=webData.getNumberOfTodaysLoads() %></TD>
		</TR>
	</TBODY>
</TABLE>
<br/>
<TABLE width="100%"  cellspacing="0" cellpadding="0" border="1">
	
		<TR>
			
						<TH align="right" width="6%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="date"/>
														</portletAPI:createURI>'>Date</A>
														<c:choose>
							<c:when test="${sortBean.dateAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.dateDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose>
					</Th>
						<TH align="center" width="11%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="originCity"/>
						</portletAPI:createURI>'>Origin</A><c:choose>
							<c:when test="${sortBean.originCityAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.originCityDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose>
						</th>
						<TH class="st" align="Right" width="3%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="originState"/>
														</portletAPI:createURI>'>ST</A><c:choose>
							<c:when test="${sortBean.originStateAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.originStateDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose>
					</Th>
						<TH align="center" width="13%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="destinationCity"/>
														</portletAPI:createURI>'>Destination
			</A><c:choose>
							<c:when test="${sortBean.destinationCityAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.destinationCityDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose></TD>
					</Th>
						<TH align="right" class="st" width="3%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="destinationState"/>
														</portletAPI:createURI>'>ST</A><c:choose>
							<c:when test="${sortBean.destinationStateAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.destinationStateDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose></TD>
					</Th>
						<TH align="right" width="7%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="commodityCode"/>
														</portletAPI:createURI>'>Freight</A><c:choose>
							<c:when test="${sortBean.commodityCodeAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.commodityCodeDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose>
					</Th>
						<TH align="right" width="6%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="equipmentId"/>
														</portletAPI:createURI>'>Trailer</A><c:choose>
							<c:when test="${sortBean.equipmentIdAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="5" height="25">
							</c:when>
							<c:when test="${sortBean.equipmentIdDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="5" height="25">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="7"
									height="25">
							</c:otherwise>
						</c:choose></Th>
						<TH align="right" width="7%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="rate"/>
														</portletAPI:createURI>'>Rate</A><c:choose>
							<c:when test="${sortBean.rateAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.rateDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose></Th>
						<TH align="right" width="6%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="rateBasisCode"/>
														</portletAPI:createURI>'>Basis</A><c:choose>
							<c:when test="${sortBean.rateBasisCodeAsc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.rateBasisCodeDec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose></Th>
						<TH align="center" width="10%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="detail1"/>
														</portletAPI:createURI>'>Detail 1</A><c:choose>
							<c:when test="${sortBean.detail1Asc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.detail1Dec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:otherwise>
								<IMG border="0"
									src='<%= response.encodeURL("/images/blank.gif") %>' width="14"
									height="13">
							</c:otherwise>
						</c:choose></Th>
						<TH align="center" width="11%"><A
							href='<portletAPI:createURI>
															<portletAPI:URIAction name="detail2"/>
														</portletAPI:createURI>'>Detail
						2</A><c:choose>
							<c:when test="${sortBean.detail2Asc}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/up_arrow.gif") %>'
									width="14" height="13">
							</c:when>
							<c:when test="${sortBean.detail2Dec}">
								<IMG border="0"
									src='<%= response.encodeURL("/images/down_arrow.gif") %>'
									width="14" height="13">
							</c:when>
									<c:otherwise>
										<IMG border="0" src='<%= response.encodeURL("/images/blank.gif") %>' width="14" height="13">
									</c:otherwise>
								</c:choose>
							</Th>
							<TH align="right"  width="6%"><A href='<portletAPI:createURI>
															<portletAPI:URIAction name="agent"/>
														</portletAPI:createURI>'>Agent</A>
							
								<c:choose>
									<c:when test="${sortBean.agentAsc}">
										<IMG border="0" src='<%= response.encodeURL("/images/up_arrow.gif") %>' width="14" height="13">
									</c:when>
									<c:when test="${sortBean.agentDec}">
										<IMG border="0" src='<%= response.encodeURL("/images/down_arrow.gif") %>' width="14" height="13">
									</c:when>
									<c:otherwise>
										<IMG border="0" src='<%= response.encodeURL("/images/blank.gif") %>' width="14" height="13">
									</c:otherwise>
								</c:choose>
							</Th>
							<TH align="right" width="10.5%"><A href='<portletAPI:createURI>
															<portletAPI:URIAction name="agentPhone"/>
														</portletAPI:createURI>'>Agent Phone</A>
							
								<c:choose>
									<c:when test="${sortBean.agentPhoneAsc}">
										<IMG border="0" src='<%= response.encodeURL("/images/up_arrow.gif") %>' width="14" height="13">
									</c:when>
									<c:when test="${sortBean.agentPhoneDec}">
										<IMG border="0" src='<%= response.encodeURL("/images/down_arrow.gif") %>' width="14" height="13">
									</c:when>
									<c:otherwise>
										<IMG border="0" src='<%= response.encodeURL("/images/blank.gif") %>' width="14" height="13">
									</c:otherwise>
								</c:choose>
							</th>
			
		</TR>
	
	
		<%
		int i = 0; 
		java.util.Calendar today = java.util.Calendar.getInstance();
		%>
		<c:forEach items="${data}" var="data">
			<%
			com.admiral.bean.LoadFinderBean bean = (com.admiral.bean.LoadFinderBean)data.get(i);
			String todaysYear = String.valueOf(today.get(java.util.Calendar.YEAR)).substring(2);
			String todaysMonth = String.valueOf(today.get(java.util.Calendar.MONTH) + 1);
			String todaysDate = String.valueOf(today.get(java.util.Calendar.DAY_OF_MONTH));
			String tempDate = "1" + todaysYear;
			if(Integer.parseInt(todaysMonth) < 10) {
				tempDate = tempDate + "0" + todaysMonth;
			} else {
				tempDate += todaysMonth;
			}
			if(Integer.parseInt(todaysDate) < 10) {
				tempDate = tempDate + "0" + todaysDate;
			} else {
				tempDate += todaysDate;
			}
			
			int shade = i % 2;
			i++;
			if(shade == 0) {
			%>
				<TR bgcolor="silver">
			<%
			} else {
			%>
				</TR><TR>
			<%
			}
			
			if(bean.getEnteredDate() == Integer.parseInt(tempDate)) {
			%>
				<TD align="center" width="6%" class="todayLoad" ><c:out value="${data.date}" /></TD>
				<TD wrap class="todayLoad" align="center" width="8%"><c:out value="${data.originCity}" /></TD>
				<TD align="center" class="todayLoad" width="4%"><c:out value="${data.originState}" /></TD>
				<TD align="center" class="todayLoad" width="10%" ><c:out value="${data.destinationCity}" /></TD>
				<TD align="center" class="todayLoad" width="4%"><c:out value="${data.destinationState}" /></TD>
				<TD align="center" class="todayLoad"><c:out value="${data.commodityCode}" /></TD>
				<TD align="center" class="todayLoad"><c:out value="${data.equipmentId}" /></TD>
				<TD align="center" class="todayLoad" >$<c:out value="${data.rate}" /></TD>
				<TD align="center" width="6%" class="todayLoad" ><c:out value="${data.rateBasisCode}" /></TD>
				<TD align="center" class="todayLoad" width="12%"><c:out value="${data.detail1}" /></TD>
				<TD align="center" class="todayLoad" width="12%"><c:out value="${data.detail2}" /></TD>
				<TD align="center" width="6%" class="todayLoad" ><c:out value="${data.agent}" /></TD>
				<TD align="center" nowrap class="todayLoad" width="10%"><a href="tel:<c:out value="${data.agentPhone}" />"><c:out value="${data.agentPhone}" /></a></TD>
			<%
			} else {
			%>
				<TD align="center" width="6%"><c:out value="${data.date}" /></TD>
				<TD wrap align="center" width="8%"><c:out value="${data.originCity}" /></TD>
				<TD align="center" width="4%"><c:out value="${data.originState}" /></TD>
				<TD align="center" width="10%"><c:out value="${data.destinationCity}" /></TD>
				<TD align="center" width="4%"><c:out value="${data.destinationState}" /></TD>
				<TD align="center"><c:out value="${data.commodityCode}" /></TD>
				<TD align="center"><c:out value="${data.equipmentId}" /></TD>
				<TD align="center">$<c:out value="${data.rate}" /></TD>
				<TD align="center" width="6%"><c:out value="${data.rateBasisCode}" /></TD>
				<TD align="center" width="12%"><c:out value="${data.detail1}" /></TD>
				<TD align="center" width="12%"><c:out value="${data.detail2}" /></TD>
				<TD align="center" width="6%"><c:out value="${data.agent}" /></TD>
				<TD align="center" nowrap width="10%"><a href="tel:<c:out value="${data.agentPhone}" />"><c:out value="${data.agentPhone}" /></a></TD>
			<%
			}
			%>
			</TR>
		</c:forEach>
	
</TABLE>
</BODY></HTML>